from cgi import print_arguments
from concurrent.futures import thread
from threading import Thread
import threading
from tokenize import Imagnumber
import pygame, sys
from pygame.locals import *

#Window Dimesion
WIDTH = 1280
HEIGHT = 720

#Ship class
class NaveSpacial(pygame.sprite.Sprite):

	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.ImagegeNave = pygame.image.load('assets/ship.jpg')
		
		self.rect = self.ImagegeNave.get_rect()
		self.rect.centerx = WIDTH / 2
		self.rect.centery = HEIGHT - (self.ImagegeNave.get_height() / 2 )


		self.ListaDisparo = []
		self.Vida = True
		self.Velocidad = 20

	def movimiento(self):
		if self.Vida == True:
			if self.rect.left <= 0:
				self.rect.left = 0
			elif self.rect.right > WIDTH:
				self.rect.right = WIDTH
	def dispara(self, x, y):
		miProyectil = Proyectil(x, y)
		self.ListaDisparo.append(miProyectil)

	def dibujar(self, superficie):
		superficie.blit(self.ImagegeNave, self.rect)


#Rocket class
class Proyectil(pygame.sprite.Sprite):
	
	def __init__(self, posx, posy):
		pygame.sprite.Sprite.__init__(self)

		self.imageProyectil = pygame.image.load('assets/misil.png')
		self.rect = self.imageProyectil.get_rect()

		self.sound = pygame.mixer.Sound('assets/rocket-sound.ogg')
		self.sound.set_volume(0.02)

		self.velocidadDisparo = 1
		self.rect.top = posy
		self.rect.left = posx - (self.imageProyectil.get_width() / 2)

	def trayectoria(self):
		self.rect.top = self.rect.top - self.velocidadDisparo

	def dibujar(self, superficie):
		superficie.blit(self.imageProyectil, self.rect)
		pygame.mixer.Sound.play(self.sound)

		
		
		
		
		
#Init settings
def spaceInvader():
	pygame.init()
	ventana = pygame.display.set_mode((WIDTH, HEIGHT))
	pygame.display.set_caption("Space invader")

	ImagenFondo = pygame.image.load('assets/background.jpg')


	#Player object
	ship = NaveSpacial()
	#Rocket init position
	demoProyectil = Proyectil(WIDTH / 2  , HEIGHT - 30)

	#Game running flag
	enjuego = True

	while True:
		ship.movimiento()
		demoProyectil.trayectoria()

		for evento in pygame.event.get():
			
			if evento.type == QUIT:
				print("juego finalizado")
				pygame.quit()
				sys.exit()


			if enjuego == True:

				keys = pygame.key.get_pressed()
				if keys[pygame.K_a]:
					ship.rect.left -= ship.Velocidad
				if keys[pygame.K_d]:
					ship.rect.right += ship.Velocidad
				if keys[pygame.K_w]:
					x,y = ship.rect.center
					ship.dispara(x,y)

		
		ventana.blit(ImagenFondo,(0,0))

		if len(ship.ListaDisparo) > 0:
			for x in ship.ListaDisparo:
				x.dibujar(ventana)
				x.trayectoria()

				if x.rect.top <-10 :
					ship.ListaDisparo.remove(x)

		ship.dibujar(ventana)
		pygame.display.update()



if __name__ == "__main__":
	spaceInvader()

